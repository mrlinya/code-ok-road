//
//  AppLog.h
//  HelloWorld
//
//  Created by bo wei on 2020/2/20.
//  Copyright © 2020 kbxk. All rights reserved.
//

#ifndef AppLog_h
#define AppLog_h

#import <UIKit/UIKit.h>

@interface LogClass : NSObject
- (void) test;
@end

#endif /* AppLog_h */
